import moment from "moment";

export default {
  created() {
    moment.locale("id");
  },
  filters: {
    dateFormat: date => {
      return moment(date).format("Do MMMM, YYYY");
    }
  }
};
