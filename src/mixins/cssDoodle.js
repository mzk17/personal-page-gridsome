export default {
  data() {
    return {
      minSize: ""
    };
  },
  /**
   * This trick need to do if you want to utilise
   * 'window' global object. Because it should
   * not executed on server
   */
  mounted() {
    if (!process.isClient) return;
    var cssDoodle = require("cssDoodle");
    this.minSize = `${window.innerWidth}px`;
  },
  methods: {
    updateDoodle(event) {
      event.target.update();
    }
  }
};
