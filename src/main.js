import "~/assets/scss/main.scss";
import "highlight.js/styles/solarized-light.css";
import highlightjs from "./services/directives/highlight";
import store from "./store";
import Vuex from "vuex";

export default function(Vue, { router, head, isClient, appOptions }) {
  Vue.use(Vuex);
  Vue.use(highlightjs);

  appOptions.store = new Vuex.Store(store);

  // Global meta tag
  head.htmlAttrs = { lang: "id" };
  head.meta.push({
    name: "description",
    content:
      "Situs web pribadi Muhammad Izzuddin Al Fikri. Profil, portofolio, dan berbagai tulisan mengenai profesi, hobi, ataupun sekedar berbagi pelajaran kisah hidup dituangkan kedalam situs web ini."
  });

  // Set default layout as a global component
  // Vue.component("Layout", DefaultLayout);
}
