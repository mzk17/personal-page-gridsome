export default {
  state: {
    theme: "light"
  },

  getters: {
    activeTheme(state) {
      return state.theme;
    }
  },

  mutations: {
    changeTheme(state, chosenTheme) {
      state.theme = chosenTheme;
    }
  }
};
