import MzkLogo from "./MzkLogo.vue";
import GmailLogo from "./GmailLogo.vue";
import TwitterLogo from "./TwitterLogo.vue";
import InstagramLogo from "./InstagramLogo.vue";
import FacebookLogo from "./FacebookLogo.vue";

const logo = {
  MzkLogo: MzkLogo,
  GmailLogo: GmailLogo,
  TwitterLogo: TwitterLogo,
  InstagramLogo: InstagramLogo,
  FacebookLogo: FacebookLogo
};

export default logo;
