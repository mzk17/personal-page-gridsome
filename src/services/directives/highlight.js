"use strict";

var hljs = require("highlight.js/lib/highlight");
var javascript = require("highlight.js/lib/languages/javascript");
var php = require("highlight.js/lib/languages/php");

let checkClass = classList => {
  return classList.contains("php;") || classList.contains("javascript;");
};

var highlightjs = {};
var binding = function(el, binding) {
  // on first bind, highlight all targets
  let targets = el.querySelectorAll("pre");
  targets.forEach(target => {
    // Look for any preformatted block that has
    // Wordpress classes convention
    if (checkClass(target.classList)) {
      // Create code DOM element
      let codeEl = document.createElement("code");

      // Get the code content from pre element
      let code = target.textContent;

      // Fill the new code element from pre element
      codeEl.innerHTML = code;

      // Change content of default pre DOM from wordpress
      target.innerHTML = "";
      target.appendChild(codeEl);

      if (binding.value) {
        codeEl.textContent = binding.value;
      }

      hljs.highlightBlock(codeEl);
    }
  });
};
highlightjs.install = function install(Vue) {
  hljs.registerLanguage("javascript", javascript);
  hljs.registerLanguage("php", php);

  Vue.directive("highlightjs", {
    deep: true,
    bind: binding,
    componentUpdated: binding
  });
};

module.exports = highlightjs;
