export default {
  type: "application/ld+json",
  json: {
    "@context": "http://www.schema.org",
    "@type": "person",
    name: "Muhammad Izzuddin al Fikri",
    jobTitle: "Web Developer",
    description:
      "I am professional Web Developer, hobbyist Graphic Designer, and Amateur Photographer",
    knowsAbout: "Web development, graphic design, photography, videography",
    url: "https://kymzk.com",
    gender: {
      "@type": "Male"
    },
    address: {
      "@type": "PostalAddress",
      streetAddress: "Jl. A. P. Pettarani Blok E23 No.7",
      addressLocality: "Makassar",
      addressRegion: "Sulawesi Selatan",
      postalCode: "90222",
      addressCountry: "Indonesia"
    },
    email: "muhammadizzuddinalfikri@gmail.com",
    telephone: "+6282296731729"
  }
};
