module.exports = {
  siteFields: ["name", "description", "URL"],
  postsFields: [
    "ID",
    "title",
    "date",
    "slug",
    "excerpt", // this one used as meta description of a post
    "content",
    "categories",
    "featured_image"
    // "tags", "like_count" <- enabled in the future development
  ],
  number: 10
};
