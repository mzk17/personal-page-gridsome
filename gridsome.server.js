const axios = require("axios");
const WP_CONFIG = require("./config/WP");
const path = require("path");

module.exports = function(api) {
  api.loadSource(async actions => {
    const { data } = await axios.get(
      `${process.env.WP_POST_API}?fields=${WP_CONFIG.postsFields.join(
        ","
      )}&number=${WP_CONFIG.number}`
    );

    // Count total posts
    let total_posts = data.found;

    // Push data from first page
    let posts = data.posts;

    // If total posts exceed the WP_CONFIG.number, then fetch others
    if (total_posts > WP_CONFIG.number) {
      const pages = Math.ceil(total_posts / WP_CONFIG.number);
      for (let i = 2; i <= pages; i++) {
        const { data } = await axios.get(
          `${process.env.WP_POST_API}?fields=${WP_CONFIG.postsFields.join(
            ","
          )}&number=${WP_CONFIG.number}&page=${i}`
        );
        // Push other posts from exceeded page(s)
        posts = posts.concat(data.posts);
      }
    }

    // Create new collection of "Post"
    let collection = actions.addCollection("Post");

    for (const item of posts) {
      let post = Object.create(item);

      // Overwrite categories to have value only it key's names
      post.categories = Object.keys(item.categories);
      post.id = post.ID;

      // Add collection
      collection.addNode(post);
    }
  });

  api.createPages(({ createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  });

  api.configureWebpack({
    resolve: {
      alias: {
        cssDoodle: path.resolve("node_modules", "css-doodle/css-doodle.min.js")
      }
    }
  });
};
