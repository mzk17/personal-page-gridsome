module.exports = {
  theme: {
    extend: {
      letterSpacing: {
        "slightly-wide": ".015em"
      }
    }
  },
  variants: {},
  plugins: []
};
