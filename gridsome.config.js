const autoprefixer = require("autoprefixer");
const tailwind = require("tailwindcss");
const purgecss = require("@fullhuman/postcss-purgecss");

const postcssPlugins = [tailwind(), autoprefixer()];
const MomentLocalesPlugin = require("moment-locales-webpack-plugin");

if (process.env.NODE_ENV === "production") postcssPlugins.push(purgecss());

module.exports = {
  siteName: "Muhammad Izzuddin al Fikri",
  templates: {
    Post: [
      {
        path: "/blog/:slug",
        component: "./src/templates/BlogPost.vue"
      }
    ]
  },
  css: {
    loaderOptions: {
      postcss: {
        plugins: postcssPlugins
      }
    }
  },
  plugins: [],
  configureWebpack: {
    plugins: [
      new MomentLocalesPlugin({
        localesToKeep: ["en", "id"]
      })
    ]
  }
};
